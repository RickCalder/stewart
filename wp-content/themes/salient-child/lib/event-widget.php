<?php
// Register and load the widget
function huronda_load_widget() {
    register_widget( 'huronda_widget' );
}
add_action( 'widgets_init', 'huronda_load_widget' );
 
// Creating the widget 
class huronda_widget extends WP_Widget {
 
function __construct() {
parent::__construct(
 
// Base ID of your widget
'huronda_widget', 
 
// Widget name will appear in UI
__('Huronda Events Widget', 'huronda_widget_domain'), 
 
// Widget description
array( 'description' => __( 'Huronda Events Widget', 'huronda_widget_domain' ), ) 
);
}
 
// Creating widget front-end
 
public function widget( $args, $instance ) {
$title = 'Event Details';
 
// before and after widget arguments are defined by themes
echo $args['before_widget'];
if ( ! empty( $title ) )
echo $args['before_title'] . $title . $args['after_title'];
 
// This is where you run the code and display the output
$queried_object = get_queried_object();

if ( $queried_object ) {
    $id = $queried_object->ID;
    $date = get_field('date', $id);
    $time = get_field('time', $id);
    $cost = get_field('cost', $id);
    $note = get_field('additional_note', $id);
    $location_name = get_field('location_name', $id);
    $location = get_field('location', $id);
}
$html = '<div class="event-details">';
$html .= '<ul>';
$html .= '<li><b>Date: </b>' . $date . '</li>';
$html .= '<li><b>Time: </b>' . $time . '</li>';
$html .= '<li><b>Cost: </b>' . $cost . '</li>';
$html .= '<li><b>Location: </b>' . $location_name . ' ' . $location['address'] . '<li>';
$html .= '<li><b>' . $note . '</b></li>';
$html .= '</ul>';
$html .= '</div>';
echo $html;
echo $args['after_widget'];
}
         
// Widget Backend 
public function form( $instance ) {
if ( isset( $instance[ 'title' ] ) ) {
$title = $instance[ 'title' ];
}
else {
$title = __( 'New title', 'huronda_widget_domain' );
}
// Widget admin form
?>
<p>
<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label> 
<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
</p>
<?php 
}
     
// Updating widget replacing old instances with new
public function update( $new_instance, $old_instance ) {
$instance = array();
$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
return $instance;
}
} // Class huronda_widget ends here